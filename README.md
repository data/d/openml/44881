# OpenML dataset: wave_energy

https://www.openml.org/d/44881

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This data set consists of positions and absorbed power outputs of wave energy converters (WECs) in four real wave scenarios from the southern coast of Australia.

The data is obtained from an optimization method (blackbox optimization) with the goal of finding the optimal buoys placement.

Each instance represents wave energy returns for different placements of 16 buoys.

**Attribute Description**

1. *x[1-16]* - WECs positions (0-566)
2. *y[1-16]* - WECs positions (0-566)
3. *energy[1-16]* - WECs absorbed power (should be ignored if *energy_total* is considered as the target variable)
4. *energy_total* - total power output of the farm: Powerall, target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44881) of an [OpenML dataset](https://www.openml.org/d/44881). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44881/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44881/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44881/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

